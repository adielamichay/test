<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            
            [
                'label'=>'Urgency',
                'value'=>$model->urgency1->name  
            ],
   [ 
                'label' => 'Created By',
				'format' => 'html',
				'value' => Html::a($model->user1->name, 
					['user/view', 'id' => $model->user1->id]),  
            ],
            
            

         
            
             [
                'label'=>'Updated By',
                'value'=>$model->user2->name  
            ],
             'created_at',
            'updated_at',
            
        ],
    ]) ?>

</div>
