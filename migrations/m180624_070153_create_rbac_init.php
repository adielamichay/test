<?php

use yii\db\Migration;

/**
 * Class m180624_070153_create_rbac_init
 */
class m180624_070153_create_rbac_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
          $manager = $auth->createRole('manager');
          $auth->add($manager);
    
          $employee = $auth->createRole('employee');
          $auth->add($employee);

          $auth->addChild($manager, $employee);
         
          $createTask = $auth->createPermission('createTask');
          $auth->add($createTask);    

          $updateTask = $auth->createPermission('updateTask');
          $auth->add($updateTask);
           
          $deleteTask = $auth->createPermission('deleteTask');
          $auth->add($deleteTask);

           $updateUser = $auth->createPermission('updateUser');
          $auth->add($updateUser);
       
          $updateOwnUser = $auth->createPermission('updateOwnUser');
    
          $rule = new \app\rbac\UserRule;
          $auth->add($rule);
                  
          $updateOwnUser->ruleName = $rule->name;                
          $auth->add($updateOwnUser);                 
                                    
                  
          $auth->addChild($manager, $updateUser);
          $auth->addChild($manager, $updateTask); 
          $auth->addChild($manager, $deleteTask);
          $auth->addChild($employee, $createTask);
          $auth->addChild($employee, $updateOwnUser);
          $auth->addChild($updateOwnUser, $updateUser);
         
         
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_070153_create_rbac_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_070153_create_rbac_init cannot be reverted.\n";

        return false;
    }
    */
}
